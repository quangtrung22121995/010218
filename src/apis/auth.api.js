
const path = require('path') ;
const util = require('../util') ;


const loginController = async (req, res) => {
    const indexFile = path.join(util.rootPath, 'template', 'login.html');
    try {
        const indexContent = await util.readFile(indexFile);
        res.header('Content-Type', 'text/html');
        res.send(indexContent);
    } catch (error) {
        res.json({ error: error }) ;
    }
};

const registerController = (req, res) => {
    const registerHtml = path.join(util.rootPath, 'template', 'register.html') ;
    util.readFile(registerHtml)
    .then((content) => {
        res.header('Content-Type', 'text/html');
        res.send(content);
    })
    .catch((error) => {
        res.json({ error: error.message });
    })
};




module.exports = {
    loginController,
    registerController,
};


