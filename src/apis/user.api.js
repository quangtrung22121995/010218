const userRepo = require('../repository/users.repo');
const findAllUserController = async (req, resp) => {
  const users =await userRepo.allUser();
  resp.json(users);
};

const createUserController = (req, resp) => {
  const userForm = req.body;
  userForm.id = 'd';
  resp.json(userForm);
};

const updateUserController = (req, resp) => {
  const userId = req.params.id;
  console.log('id '+ userId);
  console.log(req.body);
  resp.json({'update': 'ok'});
};

const deleteUserController = (req, resp) => {
  const userId = req.params.id;
  console.log('delete userId', userId);
  resp.sendStatus(204);
};

const findDetailUserController = async (req,resp) => {
  const userDetail = await userRepo.findById(req.params.id);
  resp.json(userDetail);
};

module.exports = {
  createUserController,
  findAllUserController,
  updateUserController,
  deleteUserController,
  findDetailUserController,
};