const noteRepo = require('../repository/notes.repo');
const findAllNoteController = async (req, resp) => {
  const notes =await noteRepo.allUser();
  resp.json(notes);
};

const createNoteController = (req, resp) => {
  const noteForm = req.body;
  noteForm.id = 'd';
  resp.json(noteForm);
};

const updateNoteController = (req, resp) => {
  const noteId = req.params.id;
  console.log('id '+ noteId);
  console.log(req.body);
  resp.json({'update': 'ok'});
};

const deleteNoteController = (req, resp) => {
  const noteId = req.params.id;
  console.log('delete noteId', noteId);
  resp.sendStatus(204);
};

const findDetailNoteController = async (req,resp) => {
  const noteDetail = await noeRepo.findById(req.params.id);
  resp.json(noteDetail);
};

module.exports = {
  createNoteController,
  findAllNoteController,
  updateNoteController,
  deleteNoteController,
  findDetailNoteController,
};