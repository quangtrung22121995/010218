const fs = require('fs');
const path = require('path');
const untils = require('../util');

const homepageController = (req, res) => {
  const indexFile = path.join(util.rootPath, 'template', 'index.html') ;
  fs.readFile(indexFile, (err, content) => {
      if(err) {
          res.json({error: error }) ;

      } else {
          res.header('Content-Type', 'text/html');
          res.send(content);
      }
  })
};
module.exports = {
  homepageController
};