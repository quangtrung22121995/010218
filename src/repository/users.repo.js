const path = require('path');
const utils = require('../util');
class UserRepo {
  constructor() {}
  async allUsers() {
    const userJsonpath = path.join(utils.roosPath, 'data', 'users.json');
    const userContent = await utils.readFile(userJsonpath);
    const userJson = JSON.parse(userContent);
    return userJson;
  }

  async findById(userId) {
    const users = await this.allUsers();
    const userDatail = users.find(user => user.id === userId);
    return userDetail;
  }
}
const userRepo = new UserRepo();
module.exports = userRepo;