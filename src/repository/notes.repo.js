const path = require('path');
const utils = require('../util');
class NoteRepo {
  constructor() {}
  async allNotes() {
    const noteJsonpath = path.join(utils.roosPath, 'data', 'notes.json');
    const noteContent = await utils.readFile(noteJsonpath);
    const noteJson = JSON.parse(noteContent);
    return noteJson;
  }

  async findById(noteId) {
    const notes = await this.allNotes();
    const noteDatail = users.find(note => note.id === noteId);
    return noteDetail;
  }
}
const noteRepo = new NoteRepo();
module.exports = noteRepo;