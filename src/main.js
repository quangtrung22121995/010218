const express = require('express') ;
const utils = require('./util');
const app = express() ;
const path  = require('path');
const bodyParser = require('body-parser');
const homeApis = require('./apis/home.api');
const authApis = require('./apis/auth.api');
const userApis = require('./apis/user.api');
const noteApis = require('./apis/note.api')
app.use(bodyParser.urlencoded({ extended: false }))
app.use(bodyParser.json())
app.get('/', homeApis.homepageController);
app.get('/login', authApis.loginController);
app.get('/register', authApis.registerController);
app.get('/users', userApis.findAllUserController);

app.post('/users', userApis.createUserController);
app.get('/users/:id', userApis.findDetailUserController);
app.put('/users/:id', userApis.updateUserController);
app.delete('/users/:id', userApis.deleteUserController);
app.get('/notes', noteApis.findAllNoteController);
app.post('/notes', noteApis.createNoteController);
app.get('/notes/:id', noteApis.findDetailNoteController);
app.put('/notes/:id', noteApis.updateNoteController);
app.delete('/notes/:id', noteApis.deleteNoteController);

const port = +process.env.PORT || 6060;

app.listen(port, (error) => {
    if (error) {
        return console.error('run server got an error', error) ;
    }
    console.log(`Server listening ${port}`) ;
}) ;